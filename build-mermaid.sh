#!/usr/bin/env sh

for file in $(find ./chapters -name "*.mmd"); do
  mmdc -p puppeteer-config.json -i $file -o $file.png -t default -b transparent
done