#!/usr/bin/env bash

inotifywait -m $(echo chapters/*/) -e close_write |
  while read path action file; do
    if [[ "$file" =~ .*mmd$ ]]; then
      mmdc -p puppeteer-config.json -i $path/$file -o $path/$file.png -t default -b transparent
    fi
  done