\chapter{Results}
\label{chapter3}

I divided my work into three code sprints. This allowed the project scope and goals to be flexible, with the evolution of aims as the sprints progressed. The sprints were as follows:

\begin{enumerate}
  \item PNG Generation using Simplex Noise and C++
  \item Programmatically generating a Perlin Noise based height-mesh using Unity3D
  \item Generating a map of cells using Wave Function Collapse and combining with Perlin Noise
\end{enumerate}

\section{Sprint 1. PNG Generation}

For the first sprint, I created a top-down image of a height-map using \mintinline{c++}{stb_image} for image generation and Simplex Noise for the height generation. Simplex noise offers an open-source alternative to Perlin Noise, useable outside of Unity3D's built-in function, making it ideal for use in simple C++ programs.

\subsection{Implementation}

A 1-Dimensional \mintinline{c++}{vector<double>} represents the pixels in the height-map. Each value is between 0 and 255, representing greyscale values from black to white. This program uses smcameron's \mintinline{c++}{open-simplex-noise-in-c} \cite{cameron2014simplexc}, which generates values between -1 and 1. I linearly interpolated these values to the range of 0 to 1 by adding 1 and dividing by 2.

\begin{minted}{c++}
for (int y = 0; y < SIZE; y++) {
  for (int x = 0; x < SIZE; x++) {
    double noise =
        open_simplex_noise2(osn_ctx, round_to(x, CELL_SIZE) / SCALE,
                            round_to(y, CELL_SIZE) / SCALE);
    noiseMap[y * SIZE + x] = (noise + 1.) / 2.;
  }
}
\end{minted}

The algorithm then generates an image using \mintinline{c++}{stb_image_write_png} and saved to a fixed location.

\begin{minted}{c++}
std::vector<uint8_t> to_255(const std::vector<double> &data) {
  std::vector<uint8_t> result(data.size());
  for (size_t i = 0; i < data.size(); ++i)
    result[i] = std::floor(data[i] * 255);
  return result;
}

// Output to the PNG
stbi_write_png("test.png", SIZE, SIZE, 1, to_255(noiseMap).data(),
               SIZE * sizeof(uint8_t));
\end{minted}

Configuration is done in compile-time, using the following parameters:

\begin{minted}{c++}
static const int SIZE = 512;
static const int CELL_SIZE = 8;
static const double SEED = 456.789;
static const double SCALE = 25.;
\end{minted}

\subsection{Outcome}

The final result is a simple noise map image with compile-time configurable parameters. Figure \ref{fig:simplex-png} shows how the noise values can be averaged to create buildings.

\begin{figure}[ht]
  \centering
  \subfloat[\centering 512x512 PNG output with \mintinline{c++}{CELL_SIZE = 1}]{{\includegraphics[width=0.4\linewidth]{chapters/chapter3/simplex-base.png}}}
  \qquad
  \subfloat[\centering Building generation using \mintinline{c++}{CELL_SIZE = 8}]{{\includegraphics[width=0.4\linewidth]{chapters/chapter3/simplex-cells.png}}}

  \caption{Sprint 1: Simplex noise demonstrations}
  \label{fig:simplex-png}
\end{figure}

The outputs of this program demonstrate how individual pixels on a height-map are averaged together to simulate the look of buildings. The algorithm generates the second image with the same seed as the first image but with samples taken less frequently to be appropriate for buildings.

\subsection{Shortcomings and aims for future sprints}

The time complexity of the for loop is less than ideal. If the \mintinline{c++}{CELL_SIZE} is above 1, the simplex noise function is executed for multiple pixels that we know will output the same value. Take, for example, the samples for $(8,0)$ and $(9,0)$ and \mintinline{c++}{CELL_SIZE = 8}. The snippet \mintinline{c++}{round_to(x, CELL_SIZE) / SCALE} is the same value for both \mintinline{c++}{x = 8} and \mintinline{c++}{x = 9}, so we know that the noise value for both pixels will be the same, making the second call to \mintinline{c++}{open_simplex_noise2} unnecessary. This issue was important to be addressed in future sprints as the defecit in time complexity will increase exponentially when taken into 3 dimensions.

A way to remedy this would be to check if \mintinline{c++}{x} or \mintinline{c++}{y} is a multiple of \mintinline{c++}{CELL_SIZE} and if so, use the value from the previous pixel. This would decrease time complexity for larger \mintinline{c++}{CELL_SIZE} values. The following pseudocode demonstrates the change:

\begin{minted}{c++}
if (x % CELL_SIZE == 0 && y % CELL_SIZE == 0)
  noise = open_simplex_noise2(osn_ctx, round_to(x, CELL_SIZE) / SCALE,
                              round_to(y, CELL_SIZE) / SCALE);
\end{minted}

The output PNG can be configured using compile-time parameters. However, this is limited to the image's cell size, seed, and resolution. In Chapter \ref{chapter1}, Fig. \ref{fig:perlin-demo}, I showed a demonstration of layering noise maps to produce a more complex output. This could've been used to generate building-type zones in this sprint and should be used in future sprints.

\section{Sprint 2. Perlin Noise Meshes}

This sprint achieves the following goals

\begin{enumerate}
  \item Take the previous sprints approaches to Perlin noise into three dimensions.
  \item Use configurable cell sizes to group noise values into buildings.
  \item Use multiple layers of Perlin Noise to determine the zone type of each cell.
  \item Assign density values to each cell based on the zone type and another layer of Perlin Noise.
  \item Use a density falloff parameter to create a bias towards higher density towards the city's centre.
\end{enumerate}

Because of the need for a three-dimensional mesh, Unity3D was used for the generation. As discussed in Chapter \ref{chapter2}, this was the chosen 3D renderer because of its ease of use and powerful scriptability.

\subsection{Implementation}

User-built classes in Unity take the form of \mintinline{csharp}{MonoBehaviour} components that can be attached to \mintinline{csharp}{GameObject} objects. As described in section \ref{subsection:classes}, I created an overarching \mintinline{csharp}{MeshCreator} component that attaches to an empty \mintinline{csharp}{GameObject}.

The \mintinline{csharp}{GenerateCity} method is responsible for creating the mesh. It iterates over each cell in the map, creating a new \mintinline{csharp}{GameObject} called a chunk. It does this by calling \mintinline{csharp}{GenerateChunk}

\begin{minted}{csharp}
public void GenerateCity()
{
  // Setup code (see repo for the complete snippet)
  // ...

  Debug.Log("Building Chunks");
  for (float x = 0; x < _mapDataGenerator.Width; x += chunkSize)
  for (float y = 0; y < _mapDataGenerator.Height; y += chunkSize)
    GenerateChunk(new Vector2Int(Mathf.FloorToInt(x), Mathf.FloorToInt(y)));
}
\end{minted}

The \mintinline{csharp}{GenerateChunk} method looks as follows.

\begin{minted}{csharp}
private WfcCell GenerateChunk(Vector2Int position)
{
  float heightValue = _mapDataGenerator.GetBuildingHeight(position.x, position.y,
                                                          chunkSize, chunkSize);
  Color chunkColor = _mapDataGenerator.GetBuildingColor(position.x, position.y,
                                                        chunkSize, chunkSize);

  // Copy the cell into this scene
  GameObject cell = Instantiate(squareChunk, transform);
  cell.name = $"{origCell.name} ({position.x}, {position.y})";

  // Set the cell's position in the grid
  Transform cellTransform = cell.transform;
  cellTransform.position = new Vector3(position.x, 0, position.y);
  cellTransform.localScale = new Vector3(chunkSize / 2.0f,
                                         chunkSize / 2.0f,
                                         heightValue);

  // Set the cell's colour
  MeshRenderer meshRenderer = cell.GetComponent<MeshRenderer>();
  Material cellMaterial = new(meshRenderer.sharedMaterial);
  cellMaterial.color = chunkColor;
  meshRenderer.material = cellMaterial;

  return cell;
}
\end{minted}

You can see there's another instance called \mintinline{csharp}{_mapDataGenerator} working here. This instance's implementation is similar to that of the first sprint. However, it's more abstracted, taking the position of a building as an input rather than specific coordinates and returning its height value and colour. The main methods are \mintinline{csharp}{GetBuildingHeight}, \mintinline{csharp}{GetBuildingColor}, and \mintinline{csharp}{GetBuildingZone}.

\begin{minted}{csharp}
private Zone GetBuildingZone(float x, float y, float width, float height)
{
  const float zoneSize = 30;
  float xw = (x + width / 2) / zoneSize;
  float yh = (y + height / 2) / zoneSize;

  float closenessToEdge =
      (Mathf.Abs(Mathf.InverseLerp(0, Width, x + width / 2) * 2 - 1) +
      Mathf.Abs(Mathf.InverseLerp(0, Height, y + height / 2) * 2 - 1)) / 2;

  float falloff = Sigmoid(closenessToEdge, 3, DensityFalloff);

  float residentialSeed = Seed * 300;
  float residentialNoise = Mathf.PerlinNoise(residentialSeed + xw, residentialSeed + yh);
  float commercialSeed = Seed * 200;
  float commercialNoise = Mathf.PerlinNoise(commercialSeed + xw, commercialSeed + yh);
  float densitySeed = Seed * 100;
  float densityNoise = Mathf.PerlinNoise(densitySeed + xw, densitySeed + yh);

  if (residentialNoise > commercialNoise)
  {
      if (densityNoise >= ResidentialDensityThreshold / falloff)
        return Zone.HighDensityResidential;
      else return Zone.LowDensityResidential;
  } else
  {
      if (densityNoise >= CommercialDensityThreshold / falloff)
        return Zone.HighDensityCommercial;
      else return Zone.LowDensityCommercial;
  }
}
public float GetBuildingHeight(float x, float y, float width, float height)
{
  Vector2 weights = zoneWeights[GetBuildingZone(x, y, width, height)];
  return Mathf.Lerp(weights.x, weights.y,
    Mathf.PerlinNoise(Seed + x + width / 2, Seed + y + height / 2));
}

public Color GetBuildingColor(float x, float y, float width, float height)
{
  Zone z = GetBuildingZone(x, y, width, height);
  return z switch
  {
    Zone.HighDensityCommercial => Color.blue,
    Zone.LowDensityCommercial => Color.cyan,
    Zone.HighDensityResidential => Color.yellow,
    Zone.LowDensityResidential => Color.green,
    _ => new Color(0.5f, 0.5f, 0.5f),
  };
}
\end{minted}

These three methods work together to determine a cell's zone type, density and height, which is passed back to the \mintinline{csharp}{GenerateChunk} method.

\subsection{Outcome}

Figure \ref{fig:perlin-mesh} shows the mesh generated from the Perlin Noise algorithm.

\begin{figure}[ht]
  \centering
  \includegraphics[width=0.5\linewidth]{chapters/chapter3/sprint-2.png}
  \caption{Sprint 2. Perlin Noise Meshes}
  \label{fig:perlin-mesh}
\end{figure}

The dark blue and yellow cells represent high density, and you can see that they're bundled together, akin to how an actual city would look. Meanwhile, the lighter coloured cells represent lower density and thus have a much shorter height.

\subsection{Shortcomings and aims for future sprints}

The output mesh is not very realistic, with harsh colours and few decorative details. However, it shows a functional representation of zoning and how zones can be placed to be well-distributed throughout a city.

The generation of roads and other infrastructure spanning multiple cells is missing from this sprint. Perlin Noise alone couldn't be used to generate larger structures because it cannot create cells based on the state of neighbouring cells. In the next sprint, the program introduces a Wave Function Collapse algorithm to achieve this task.

\section{Sprint 3. Wave Function Collapse}

The third sprint uses the Wave Function Collapse algorithm to choose which type of cell to place at a given position. This results in a map of cells with the same zoning rules as the previous sprint. However, the cells' shape changes based on their neighbours' state, creating long roads that cut through the city.

\subsection{Implementation}

This sprint adds a new set of classes, \mintinline{csharp}{WfcMap}, \mintinline{csharp}{WfcCell}, and \mintinline{csharp}{PossibleCells}. The general interaction between these classes are described in Figure \ref{fig:class-diagram}.

Six meshes are created in Blender and loaded into Unity into the \mintinline{csharp}{Meshes} scene, which gets loaded by the \mintinline{csharp}{SceneManager} when generating the city.

The \mintinline{csharp}{MeshGenerator} class is modified to also include the \mintinline{csharp}{WfcMap} class.

\begin{minted}{csharp}
public void GenerateCity()
{
  WfcCell[] cells = SceneManager.GetSceneByName("Meshes").GetRootGameObjects()
    .Select(c => c.GetComponent<WfcCell>()).ToArray();
  _wfcMap = new WfcMap(cells, Mathf.CeilToInt(citySize / (float)chunkSize),
            Mathf.CeilToInt(citySize / (float)chunkSize), seed);
  // Same code as before
  // ...
}
\end{minted}

Now in place of the \mintinline{csharp}{Instantiate} call in \mintinline{csharp}{GenerateChunk}, the \mintinline{csharp}{WfcMap} class is used to get the cell at a given position.

\begin{minted}{csharp}
// Copy the cell into this scene
WfcCell origCell = _wfcMap.GetCellAt(position.x / chunkSize, position.y / chunkSize);
WfcCell cell = Instantiate(origCell, transform);
cell.name = $"{origCell.name} ({position.x}, {position.y})";
\end{minted}

The Wave Function Collapse algorithm is implemented in the \mintinline{csharp}{WfcMap} class, which owns the \mintinline{csharp}{WfcCell} and \mintinline{csharp}{PossibleCells} classes. The important functions are \mintinline{csharp}{Generate}, \mintinline{csharp}{GetCellAt}, \mintinline{csharp}{SetCellAt} and \mintinline{csharp}{CollapseGrid}.

\begin{minted}{csharp}
public WfcCell GetCellAt(int x, int y)
{
  if (Generated) return Grid[x, y][0];

  Generate();
  Generated = true;

  return Grid[x, y][0];
}
\end{minted}

\mintinline{csharp}{GetCellAt} calls the \mintinline{csharp}{Generate} method if the map detects that it hasn't fully collapsed yet. The \mintinline{csharp}{Generate} method will then loop indefinitely until the map is fully collapsed. This check ensures that the map has fully collapsed before any cell is accessed when calling \mintinline{csharp}{GetCellAt}. If \mintinline{csharp}{GetCellAt} is called when the map has already collapsed, then it won't call \mintinline{csharp}{Generate} and will return the cell at the given position.

\begin{minted}{csharp}
private void Generate()
{
  // Initialize Random here to ensure consistency
  Random.InitState(Seed);

  // Choose cells to set using a semi-random process
  while (true)
  {
    // Choose a cell with the lowest number of possibilities
    int lowestDomain = int.MaxValue;
    Vector2Int lowestDomainPosition = new(-1, -1);
    for (int y = 0; y < Height; y++)
    for (int x = 0; x < Width; x++)
      if (Grid[x, y].Count != 1 && Grid[x, y].Count < lowestDomain)
      {
        lowestDomain = Grid[x, y].Count;
        lowestDomainPosition = new Vector2Int(x, y);
      }

      // If all cells have a domain of 1, the grid is fully collapsed, so we can stop
      if (lowestDomain == int.MaxValue
       || lowestDomainPosition == new Vector2Int(-1, -1))
            break;

      // Set the cell at the lowest domain position
      Grid = SetCellAt(lowestDomainPosition.x, lowestDomainPosition.y, Grid);
  }
}
\end{minted}

This is where the \mintinline{csharp}{SetCellAt} and \mintinline{csharp}{CollapseGrid} methods come in. \mintinline{csharp}{SetCellAt} sets the cell at the given position to a random cell from its list of viable cells. It then calls \mintinline{csharp}{CollapseGrid} to check any neighbouring cells that may now need to update their list of viable cells based on the newly set cell.

\begin{minted}{csharp}
private PossibleCells[,] SetCellAt(int x, int y, PossibleCells[,] grid)
{
  // Clone the grid
  PossibleCells[,] gridClone = new PossibleCells[Width, Height];
  for (int yy = 0; yy < Height; yy++)
  for (int xx = 0; xx < Width; xx++)
    gridClone[xx, yy] = new PossibleCells(grid[xx, yy]);

  // Try every possible cell until one works
  while (!gridClone[x, y].IsEmpty)
  {
    // Get a random cell from the list
    List<WfcCell> weightedList = new();

    foreach (WfcCell cell in gridClone[x, y])
      weightedList.AddRange(Enumerable.Repeat(cell, (int)cell.weight));
    int index = Random.Range(0, weightedList.Count);
    WfcCell possibleCell = weightedList[index];

    // Collapse the grid
    PossibleCells[,] possibleGrid = CollapseGrid(new Vector2Int(x, y),
                                                 possibleCell, gridClone);
    if (possibleGrid != null)
      return possibleGrid;

    // Remove the cell from the list
    gridClone[x, y].Remove(possibleCell);
  }

  return null;
}
\end{minted}

The implementation uses clones of the original grid to run through many different possibilities recursively without affecting the original grid. Then, if the current iteration turns out to be impossible (indicated by the final \mintinline{csharp}{return null;} statement), the outcome can be ignored. The \mintinline{csharp}{CollapseGrid} method is in charge of checking the neighbouring cells to see if they need to be updated. If they do, it updates the neighbours' neighbours and so on. This is done through recursive calls of \mintinline{csharp}{CollapseGrid}

\subsection{Outcome}

\begin{figure}[ht]
  \centering
  \includegraphics[width=0.5\linewidth]{chapters/chapter3/sprint-3.png}
  \caption{Sprint 3. Wave Function Collapse}
  \label{fig:wave-collapse}
\end{figure}

Figure \ref{fig:wave-collapse} shows the mesh being similar in appearance to the previous sprint, but roads now sweep across the grid. The density falloff system of the last sprint can also be seen here.

One of the main problems with the previous sprints was the lack of configuration options or GUI. This is remedied this time using Unity Editor panels. Each parameter can be adjusted using either sliders or check-boxes.

Figure \ref{fig:wave-collapse-density-adjustment} shows the results of the configuration adjustments. The lower density falloff allows high-density buildings to exist in the centre of the map. The effectiveness of the density falloff will be discussed in the following chapter.

\begin{figure}[ht]
  \centering
  \subfloat[\centering Lowering the density falloff]{{\includegraphics[width=0.6\linewidth]{chapters/chapter3/sprint-3-density-falloff.png}}}
  \qquad
  \subfloat[\centering Result of adjustment, higher density more prominent in the centre]{{\includegraphics[width=0.3\linewidth]{chapters/chapter3/sprint-3-density-falloff-result.png}}}
  \caption{Sprint 3. Wave Function Collapse Adjustments}
  \label{fig:wave-collapse-density-adjustment}
\end{figure}

Figure \ref{fig:wave-collapse-adjustments} shows even more possible adjustments to the parameters to produce different results. Fig. \ref{fig:wave-collapse-adjustments}a demonstrates how you can increase the size of each chunk/building, which makes the whole map seem flatter due to the building heights not growing along with them. Fig. \ref{fig:wave-collapse-adjustments}b shows how you can eliminate certain zoning types by adjusting the zoning thresholds, resulting in a commercial-only city.

\begin{figure}[ht]
  \centering
  \subfloat[\centering Increasing the chunk size, results in wider, flatter buildings]{\includegraphics[width=0.5\linewidth]{chapters/chapter3/sprint-3-chunk-size-result.png}}
  \qquad
  \subfloat[\centering Minimizing commercial threshold, maximising residential threshold. Allows for only commercial buildings]{\includegraphics[width=0.5\linewidth]{chapters/chapter3/sprint-3-commercial-result.png}}
  \caption{More adjustments to change the appearance of the map}
  \label{fig:wave-collapse-adjustments}
\end{figure}
