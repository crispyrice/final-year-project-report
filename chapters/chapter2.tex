\chapter{Methods}
\label{chapter2}

\section{Preparation}

\subsection{Methodology}

This project divides time into two-week sprints. This process allows for the rapid development of prototypes by employing knowledge learned from previous sprints to speed up development. There are three types of sprint, research, writing and programming.

\subsection{Tools \& Engines}

\subsubsection{Miscellaneous Tools}

Git is version control software of choice for everything in this project. Git repositories for both this report and the project are available on GitLab:

\begin{itemize}
  \item Project: \url{https://gitlab.com/crispyrice/final-year-project}
  \item Report: \url{https://gitlab.com/crispyrice/final-year-project-report}
\end{itemize}

The other creation tools were as follows:

\begin{itemize}
  \item Visual Studio Code: Used for writing this report and for the C++ code sprints.
  \item JetBrains Rider: Used for editing C\# scripts in the Unity projects.
  \item Blender: Used for creating meshes for the cells in the Unity projects.
  \item Unity3D: Used for the 3D code sprints. Justification for this software over similar solutions is explained in the following subsection.
\end{itemize}

\subsubsection{Unity3D vs. Bespoke OpenGL Engine}

There are a few options that I had for implementing this project.

The first was to create a C++ renderer from scratch using OpenGL and GLFW for cross-platform window management. The project would be a library included for anyone who needs to use it. This was a good option for several reasons. First, creating a minimal dependency C++ library has numerous benefits for anyone who wishes to use the project for their research. Second, using OpenGL, GLFW, and C++ ensures that the project is portable to any operating system and system architecture.

On the other hand, I feared that a C++ renderer would take up the bulk of the project work, leading to a less featured final product. Therefore, I also considered using Unity3D for graphics visualisation and procedural generation.

The main advantage of Unity3D is that it provides a complete renderer and entity component system out of the box, emphasising the project's primary goal of procedural generation. It provides a scripting system that uses C\# rather than C++, which is a subjectively more straightforward language to program. The disadvantage is that Unity3D is a significant, closed-source dependency, making it less favourable for open-source projects.

This project uses Unity3D due to the convenience of its pre-built renderer.

\section{Programming}

\subsection{City Generation Breakdown}

The city is divided into square cells (\mintinline{csharp}{WfcCell} in the codebase), which form a grid (\mintinline{csharp}{WfcGrid} in the codebase). A noise function generated using Unity's built-in Perlin noise algorithm determines the cell's height. The cell's shape is chosen from a list of possible meshes using a version of the Wave Function Collapse algorithm.

A second Perlin Noise function is used to determine the zone/biome of the cell. The biome is used to determine the cell's height and colour.

\begin{enumerate}
  \item A grid of square cells is created.
  \item The Waveform Collapse algorithm determines the building type of a cell \cite{sherratt2019wfc}.
  \item Multiple layers of Perlin noise determines the height of each cell \cite{perlin1985imagesynth}
  \item Another independent layer of Perlin noise is used to determine biomes.
  \item Biomes act as zoning rules and determine building type and maximum height values.
\end{enumerate}

With all of these steps processed, the algorithm iterates over each cell, placing the appropriate mesh.

\subsection{Classes}
\label{subsection:classes}

The map generation is broken down into two main components: a height generator and a building/shape generator. The relationship between these components can be seen in the class diagram (Fig. \ref{fig:class-diagram}). The following are brief descriptions of what each class does. Many of the algorithms mentioned will be explained in more detail further on.

\mintinline{csharp}{MeshCreator}: The Mesh Creator class is responsible for creating the cells and rendering them into a mesh. It does this by accessing both the \mintinline{csharp}{WfcMap} and \mintinline{csharp}{HeightGenerator} classes.

\mintinline{csharp}{WfcMap}: This class holds the grid of cells used by the Wave Function Collapse algorithm. When the Mesh Creator attempts to access a cell in the map, it will make sure that the cell at that position is fully collapsed and propagate the change accordingly amongst its neighbours.

\mintinline{csharp}{HeightGenerator}: This class is responsible for generating the height of each cell. It uses the Perlin Noise algorithm to create a noise map. Multiple layers of Perlin Noise are developed to determine the zoning for the particular cell.

\mintinline{csharp}{PossibleCells}: This is contained by the \mintinline{csharp}{WfcMap} and represents a collection of cells at a given grid position that is still possible to be placed there. The number of viable cells at a grid position is called the domain of that position.

\mintinline{csharp}{WfcCell}: This class represents a single cell shape. It holds the mesh and socket values for the cell. This class is instantiated multiple times as copies of it are used across the map. As far as possible, references are used to avoid unnecessary copying.

\begin{figure}[ht]
  \centering
  \includegraphics[width=0.5\linewidth]{chapters/chapter2/class-diagram.mmd.png}
  \caption{Class Diagram. The mesh creator class is attached to an empty GameObject}
  \label{fig:class-diagram}
\end{figure}

\subsection{Mesh Creation}

6 different meshes were created in Blender:

\begin{enumerate}
  \item Building
  \item Cross Junction
  \item Plaza
  \item T-Junction
  \item Straight Road
  \item Bend
\end{enumerate}

Any mesh without rotational symmetry was then also rotated as needed. Each mesh was imported into a Unity3D scene to be loaded by the engine.

\begin{figure}[ht]
  \centering
  \includegraphics[width=0.7\linewidth]{chapters/chapter2/building-type-meshes.png}
  \caption{All the building type meshes to be used by Unity3D}
\end{figure}

The modification of these meshes to fit the cell's height and shape is described in section \ref{sec:height-generation}.

\subsection{Height Generation}
\label{sec:height-generation}

The \mintinline{csharp}{HeightGenerator} class uses Ken Perlin's \cite{perlin1985imagesynth} algorithm to generate a noise map. The noise map is then used to determine the height of each cell. Perlin's noise algorithm was chosen over simplex noise due to its existing implementation in Unity3D's \mintinline{csharp}{Mathf} library.

The Perlin Noise function is written as $N(a,b)$, which returns a single value between 0 and 1. This function will generate the heights of buildings, and multiple layers will be used to create different zoning rules that will alter the appearance and size of these buildings. A building is identified by $x$ and $y$ coordinates along with a $dx$ and $dy$ value for the dimensions of the building (Fig. \ref{fig:building-height-function}). This samples the Perlin Noise value at the centre of the building.

\begin{figure}[ht]
  $$
    H(b) = N \Bigg(x + \frac{dx}{2}, y + \frac{dy}{2}\Bigg)
  $$
  \caption{Function to calculate the height of building $b$ using the Perlin Noise function $N(x,y)$}
  \label{fig:building-height-function}
\end{figure}

Each building will have a zoning type along with a zoning density. These two factors are combined to determine the building's shape, size, height, and overall appearance. The zoning types are:

\begin{enumerate}
  \item Residential
  \item Commercial
  \item Park
  \item Industrial
\end{enumerate}

Each zoning type has a noise map. The given building type is determined by whichever noise map layer has the highest value. Fig. \ref{fig:zoning-weights} shows a demonstration of this.

\begin{figure}[ht]
  \centering
  \includegraphics[width=0.7\linewidth]{chapters/chapter2/zoning-weights.png}
  \caption{Example of choosing the zone type based on layered noise maps}
  \label{fig:zoning-weights}
\end{figure}

The \mintinline{csharp}{HeightGenerator} class assigns a density type to each cell based on whether its density value is above or below a given threshold. The density is determined using a single pass of the Perlin Noise algorithm, assigning each cell a density value between 0 and 1. Each zone type can be either of high density or low density. For example, a residential cell with a density value of 0.7 and a threshold of 0.5 is given a high-density type.

The combination of zone and density types differentiate the cells in visual appearance. In this project, the cell types determine an individual cell's colour and minimum/maximum height.

\subsubsection{Density Falloff}

One key component of many large cities is a distinct, high-density city centre. A density falloff value is used to provide a bias towards higher density buildings in the centre of the map. This calculates the distance a given cell is from the centre of the map and applies additional weighting to it.

This result is achieved by calculating an inverse linear interpolation using Unity's \mintinline{csharp}{Mathf} library and normalising it to a value between 0 and 1.

$$
  d_x = | \text{InvLerp}_{0,\frac{w}{2}}(x) |
$$

The same is performed for $d_y$. These values are then passed through a sigmoid function and averaged with the user-provided density weightings.

\subsection{Wave Function Collapse}

One challenge of this project is to create a realistic network of buildings connected by roads with appropriate capacities. This is where less random but natural-looking algorithms are still needed, one that can build road networks that diverge and turn according to a set of rules.

The Wave Function Collapse algorithm described by M. Gumin \cite{gumin2016wfc} is used to choose a possible type of object to go in one cell in a grid of cells, according to rules defined by the user. We can give this algorithm some types of roads and some laws that restrict which cells can be adjacent. For example, Fig. \ref{fig:wfc-road1} shows the rules for the first type of road.

\begin{figure}[ht]
  \centering
  \subfloat[Two road types that are allowed by the rules to be adjacent]{\includegraphics[width=0.2\linewidth]{chapters/chapter2/road-centre.png}
    \includegraphics[width=0.2\linewidth]{chapters/chapter2/road-straight.png}}
  \qquad
  \subfloat[Two road types that aren't allowed to be adjacent]{\includegraphics[width=0.2\linewidth]{chapters/chapter2/road-centre.png}
    \includegraphics[rotate=90,width=0.2\linewidth]{chapters/chapter2/road-straight.png}}
  \caption{Examples of road types that could and couldn't be adjacent}
  \label{fig:wfc-road1}
\end{figure}

In this project, \mintinline{csharp}{WfcCell} objects contain a set of sockets for \mintinline{csharp}{Up}, \mintinline{csharp}{Down}, \mintinline{csharp}{Left}, and \mintinline{csharp}{Right} connections. These are used to determine which cells are adjacent to a given cell. In figure \ref{fig:wfc-road1}, the straight road's \mintinline{csharp}{Left} and \mintinline{csharp}{Right} sockets contain the value 1, and the \mintinline{csharp}{Up} and \mintinline{csharp}{Down} sockets contain the value 0. This allows the straight road to be connected to any cell with a value of 1 in the \mintinline{csharp}{Left} or \mintinline{csharp}{Right} socket, or a value of 0 in the \mintinline{csharp}{Up} or \mintinline{csharp}{Down} socket.

\subsubsection{Implementation of the Wave Function Collapse}

The implementation of Wave Function Collapse will need to be bespoke to fit the needs of a given project. The basis is to use a recursive set of function calls to check the possible states of a given cell and propagate the result of this change against the adjacent cells. This can be thought of as traversing a tree, as shown in Fig. \ref{fig:wfc-tree}.

\begin{figure}
  \centering
  \includegraphics[width=0.5\linewidth]{chapters/chapter2/wfc-tree.png}
  \caption{Possible adjacencies for a cell in the Wave Function Collapse tree}
  \label{fig:wfc-tree}
\end{figure}