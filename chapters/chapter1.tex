\chapter{Introduction and Background Research}

% You can cite chapters by using '\ref{chapter1}', where the label must
% match that given in the 'label' command, as on the next line.
\label{chapter1}

% Sections and sub-sections can be declared using \section and \subsection.
% There is also a \subsubsection, but carefully consider if you need
% so many layers of section structure.
\section{Introduction \& Outline}

Simple noise algorithms have aided the procedural generation of the natural world for decades. It has helped us better understand how many natural phenomena occur, from providing the base model for simulating rainfall carving shapes into mountainsides to modelling movements of tectonic plates on a planetary scale to understand the effects of collisions on terrain.

My project aims to apply this procedural generation to the creation of cities. While cities may not be considered natural, they grow similarly to the natural world. Because of this, procedural generation techniques intended for nature should still apply to the creation of cities.

% Must provide evidence of a literature review. Use sections
% and subsections as they make sense for your project.
\section{Use of Procedural Generation Algorithms}

\subsection{Ken Perlin - An Image Synthesiser}

One of the fundamental foundations for procedural generation is the use of Ken Perlin's Noise algorithm, formally described in his 1985 paper, An Image Synthesiser \cite{perlin1985imagesynth}. The algorithm samples points regularly on a grid, assigning a random gradient vector to each, and calculates the dot product. By doing this, it creates a smooth, continuous, and significantly random noise image (Fig. \ref{fig:perlin-demo}).

\begin{figure}[ht]
  \centering
  \includegraphics{chapters/chapter1/perlin-demo.png}
  \caption{Perlin Noise image using 16 sample points \cite{blackears2015noisemaker}}
  \label{fig:perlin-demo}
\end{figure}

Perlin later developed an updated noise algorithm called Simplex Noise, the algorithm I eventually implemented in my project.

Perlin \& Simplex Noise has been used in many practical applications, including interpreting the black and white values as terrain heights - called a height-map - to generate realistic-looking, natural terrain. Multiple height-maps can then be layered on top of each other to create a more coarse look (Fig \ref{fig:perlin-2-layered-demo} \& \ref{fig:perlin-4-layered-demo}).

\begin{figure}[ht]
  \centering
  \includegraphics{chapters/chapter1/perlin-2-layered-demo.png}
  \caption{A similar noise image with two layers \cite{blackears2015noisemaker}}
  \label{fig:perlin-2-layered-demo}
\end{figure}

\begin{figure}[ht]
  \centering
  \includegraphics{chapters/chapter1/perlin-4-layered-demo.png}
  \caption{A similar noise image with four layers \cite{blackears2015noisemaker}}
  \label{fig:perlin-4-layered-demo}
\end{figure}

A 2-Dimensional height map can be converted into a 3-Dimensional mesh to be rendered by a 3D graphics engine; this was the use-case for Perlin's original algorithm, in which he won an Academy Award for its use in the movie TRON.

\subsubsection{Using Perlin Noise - Hydraulic Erosion}

In more recent research, Perlin Noise has been used as the basis for other algorithms, such as Benes et al.'s hydraulic erosion algorithm \cite{benes2006hydraulicerosion}, which simulates water droplets and their trajectory across the terrain to simulate the natural formation of rivers and valleys. This algorithm is implemented in Unity3D by Hans Theobald Beyer \cite{beyer2015heimpl}, demonstrating how Perlin Noise is built upon to produce more realistic terrain than that seen in TRON.

\subsubsection{Simplex Noise, modern alternative to Perlin Noise}

In 2001, Ken Perlin designed another noise algorithm, called Simplex Noise. It was more computationally efficient than Perlin Noise, while also producing less artifacts. Others have developed copies of Simplex Noise for open source usage, such as Stephen Cameron's \cite{cameron2014simplexc} Open Simplex Noise in C, which is a C adaptation of OpenSimplex, written in Java.

Despite Simplex Noise being subjectively superior, its usage in my project along with many others is circumstantial, due to Unity's availablility for the Perlin Noise algorithm in their built-in \mintinline{csharp}{Mathf} library. Meanwhile, the C implementation of Simplex Noise is the obvious choice for any projects built in the C family of languages.

\subsection{Wave-Function Collapse}
\label{research:wfc}

An algorithm that is more relevant to building cities is the Wave Function Collapse algorithm, developed by Maxim Gumin \cite{gumin2016wfc}. This algorithm takes a grid of cells with a finite number of possible states and rules that determine which state is allowed based on the adjacent cells' states. Each cell is considered to be in a superposition of states. Once a state is chosen for a given cell, adjacent cells' superpositions are updated to reflect the change; this is called a collapse. This process repeats until the grid has entirely collapsed.

A simple example of how this is useful is for solving Sudoku. The cells described previously fit with the cells on the 9x9 grid. Using the rule that a cell cannot be the same number as any in the same block, we can view calculate the possible numbers for each cell, a.k.a their superposition (see Fig. \ref{fig:wfc-sudoku-block}).

\begin{figure}[ht]
  \centering
  \begin{tikzpicture}
    \draw[] (0,0) grid (3,3);
    \node at (0.5,0.5) {1};
    \node at (1.5, 2.5) {2};
    \node at (2.5, 1.5) {9};
    \small \node at (1.5, 0.7) {3 4 5};
    \small \node at (1.5, 0.3) {6 7 8};
    \small \node at (2.5, 0.7) {3 4 5};
    \small \node at (2.5, 0.3) {6 7 8};
    \small \node at (0.5, 1.7) {3 4 5};
    \small \node at (0.5, 1.3) {6 7 8};
    \small \node at (0.5, 2.7) {3 4 5};
    \small \node at (0.5, 2.3) {6 7 8};
    \small \node at (2.5, 2.7) {3 4 5};
    \small \node at (2.5, 2.3) {6 7 8};
    \small \node at (1.5, 1.7) {3 4 5};
    \small \node at (1.5, 1.3) {6 7 8};
  \end{tikzpicture}
  \caption{A half-solved single block of a sudoku grid with all of its superpositions}
  \label{fig:wfc-sudoku-block}
\end{figure}

In this case, the algorithm has reached a stalemate, and it must randomly choose a state for one cell and then update the superpositions of the adjacent cells. This process repeats until the grid has collapsed entirely.

\subsection{Irregular Grids}

Famous examples of procedural generation in video games such as Minecraft use a grid consisting of tesselated squares. This is great because it lets each cell be addressed by an integer $x$ and $y$ coordinate, making it easy to run complex algorithms on each cell to determine its properties.

However, more recent algorithms have been developed that use unstructured grids that produce triangular tesselations that may be considered more natural-looking. One such method is the Delaunay Triangulation \cite{farrashkhalvat2003delaunay}, which connects relatively equidistant, but randomly placed points to form uniformly equilateral triangles. Farrashkhalvat's introduction to the algorithm provides some insightful demonstrations of the result of this method.

\begin{figure}[ht]
  \centering
  \subfloat[Tesselating Voroni Regions]{\includegraphics[width=0.3\linewidth]{chapters/chapter1/voroni-tesselation.png}}
  \qquad
  \subfloat[Triangulating from the set of points $N$]{\includegraphics[width=0.3\linewidth]{chapters/chapter1/delaunay.png}}
  \caption{The two main stages of Delaunay Triangulation \cite{farrashkhalvat2003delaunay}}
  \label{fig:delaunay-tesselation}
\end{figure}

Each triangle in figure \ref{fig:delaunay-tesselation}b can be considered a chunk or cell on a grid. From here, other algorithms can determine the properties of each cell to create a unique and visually pleasing result.

However, it's not common to see cities and buildings shaped like triangles in the real world, so there needs to be a method where square blocks or cells can be placed in a triangular position of a grid. Fortunately, triangles can be grouped to form squares or hexagons, to allow cells to span multiple tesselated triangles \cite{stalberg2021townscapers}. We can then modify the coordinates of each vertex to align with the points of the triangulated grid.

\subsection{Interactively building cities}

Lipp et al. \cite{lipp2011interactivecity} created a model for interactively building cities that used procedural generation to provide feedback while a user was editing the city. The paper's main focus was to create a city model that could be edited by a user using an intuitive user interface. This involved having mouse controls for moving roads, buildings, and other nodes, while having the generation algorithms work in real-time to adapt to the changes. There's an emphasis on avoiding the use of grammar and parameters to have a much more fine-tuned control over the city's visual appearance.

One of the main issues with this approach to creating a city is that it's challenging to store the city's state after user modifications to it. One naive approach is to dump the contents of memory into a file, which could be loaded by the engine later on. The problem with this approach is that if the city is large, the file size would also be large, as unimportant data like textures and meshes would be unnecessarily stored, despite being implicitly inferred by the engine. If this project stored only its parameters to represent the city's state, the file size would be much smaller, leading to more efficient space usage.

\section{Challenges for this project}

With all this in mind, the project undertaken should be able to produce a city model that's sufficiently editable by a user, reproducible via configuration options and be able to replicate an actual city.

The key challenge here is to orchestrate Wave Function Collapse with Perlin Noise or a Perlin Noise alternative on a grid of chunks/cells to produce a city.
